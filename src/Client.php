<?php

namespace Drupal\thingspeak_api;

use GuzzleHttp\ClientInterface;

/**
 * Thingspeak API client.
 */
class Client {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Set an API key to use for several operations, if you want.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * The format.
   *
   * @var string
   */
  protected $format = 'json';

  /**
   * Constructs a Client object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * Method description.
   */
  public function writeData($data) {
    return $this->update($data);
  }

  /**
   * Write data.
   */
  public function update($data) {
    if (empty($data['api_key'])) {
      $data['api_key'] = $this->apiKey;
    }
    if (empty($data['api_key'])) {
      throw new \Exception('Missing API key for sending channel update');
    }
    $format = $this->getFormat();
    if ($format === 'json') {
      $data = json_encode($data);
    }
    $response = $this->httpClient->request('POST', $this->getUpdateUri(), [
      'body' => $data,
      'headers' => [
        'Content-Type' => $format === 'json' ? 'application/json' : 'application/x-www-form-urlencoded',
      ],
    ]);
    switch ($format) {
      case 'json':
        return json_decode($response->getBody());

      case 'xml':
        return simplexml_load_string($response->getBody());

      default:
        return $response->getBody();
    }
  }

  /**
   * Get the URI for sending updates to.
   */
  public function getUpdateUri() : string {
    $format = $this->getFormat();
    $format_suffix = ".$format";
    if (!in_array($format, ['xml', 'json'])) {
      $format_suffix = '';
    }
    return sprintf('%s/update%s', $this->getBaseUri(), $format_suffix);
  }

  /**
   * Get the actual base of the API sending.
   *
   * @todo Probably expose this somehow. Currently to override one would have
   * swap this entire service.
   */
  public function getBaseUri() : string {
    return 'https://api.thingspeak.com';
  }

  /**
   * Getter for format.
   */
  public function getFormat() : string {
    return $this->format;
  }

  /**
   * Setter for format.
   */
  public function setFormat(string $format) {
    switch ($format) {
      case 'csv':
      case 'xml':
      case 'json':
        $this->format = $format;
        break;

      default:
        // There are only two formats allowed.
        $this->format = '';
        break;
    }
  }

}
